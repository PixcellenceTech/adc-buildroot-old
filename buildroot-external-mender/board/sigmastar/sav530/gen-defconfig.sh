#!/bin/sh

set -e

SOURCE_DEFCONFIG="sigmastar_sav530_defconfig"
TARGET_DEFCONFIG="mender_sigmastar_sav530_defconfig"

if [ ! -d buildroot ]; then
    echo "Sorry, you must execute this script from the top directory"
    exit 1
fi
mkdir -p  buildroot-external-mender/configs
cp buildroot/configs/${SOURCE_DEFCONFIG} buildroot-external-mender/configs/${TARGET_DEFCONFIG}

# Remove confliction configs
sed -i '/^BR2_ROOTFS_POST_IMAGE_SCRIPT/ d' buildroot-external-mender/configs/${TARGET_DEFCONFIG}

cat buildroot-external-mender/board/common/mender-common.cfg \
    buildroot-external-mender/board/sigmastar/sav530/defconfig.cfg \
    >> buildroot-external-mender/configs/${TARGET_DEFCONFIG}
