#!/bin/sh

set -e
set -x

device_type=$(cat ${TARGET_DIR}/data/mender/device_type | sed 's/[^=]*=//')
artifact_name=$(cat ${TARGET_DIR}/etc/mender/artifact_info | sed 's/[^=]*=//')
artifact_sscript=${BR2_EXTERNAL}/board/sigmastar/sav530/Mender-ArtifactScripts/

cp  ${BUILD_DIR}/linux-custom/arch/arm/boot/uImage.xz  ${BR2_EXTERNAL}/board/sigmastar/sav530/genimage/board/i6e/boot/spinand/kernel/
cp  ${BUILD_DIR}/uboot-custom/u-boot_spinand.xz.img.bin ${BR2_EXTERNAL}/board/sigmastar/sav530/genimage/board/i6e/boot/spinand/uboot/
mv  ${BINARIES_DIR}/rootfs.tar  ${BR2_EXTERNAL}/board/sigmastar/sav530/genimage/board/i6e/boot/spinand/rootfs/
rm -rf  ${BINARIES_DIR}/*
#cp  ${BUILD_DIR}/uboot-custom/u-boot_spinand.xz.img.bin ${BINARIES_DIR}/
[ ! -d ${BINARIES_DIR}/scripts ] && mkdir -p ${BINARIES_DIR}/scripts 
[ ! -d ${BINARIES_DIR}/boot ] && mkdir -p ${BINARIES_DIR}/boot

cd ${BR2_EXTERNAL}/board/sigmastar/sav530/genimage/
make -f makefile

cd ${BINARIES_DIR}/scripts/ ; sed -i 's/tftp/fatload mmc 0:1/g' *  
cd ${BINARIES_DIR}/ ; sed -i 's/estar/dstar/g' auto_update.txt  


#device_type=$(cat ${TARGET_DIR}/data/mender/device_type | sed 's/[^=]*=//')
#artifact_name=$(cat ${TARGET_DIR}/etc/mender/artifact_info | sed 's/[^=]*=//')

#if [ -z "${device_type}" ] || [ -z "${artifact_name}" ]; then
#    echo "missing files required by Mender (/etc/mender/device_type or /etc/mender/artifact_info)"
#    exit 1
#fi

${HOST_DIR}/usr/bin/mender-artifact write rootfs-image \
    --file ${BINARIES_DIR}/rootfs.sqfs \
    --output-path ${BINARIES_DIR}/${artifact_name}.mender \
    --artifact-name ${artifact_name} \
    --script ${artifact_sscript} \
    --device-type ${device_type}

#${HOST_DIR}/usr/bin/mender-artifact write rootfs-image \
#    --update ${BINARIES_DIR}/rootfs.squashfs \
#    --output-path ${BINARIES_DIR}/${artifact_name}.mender \
#    --artifact-name ${artifact_name} \
#    --device-type ${device_type}

#GENIMAGE_TMP="${BUILD_DIR}/genimage.tmp"
#rm -rf "${GENIMAGE_TMP}"

#genimage \
#    --rootpath "${TARGET_DIR}" \
#    --tmppath "${GENIMAGE_TMP}" \
#    --inputpath "${BINARIES_DIR}" \
#    --outputpath "${BINARIES_DIR}" \
#    --config "${BR2_EXTERNAL}/board/freescale/sav530evk/genimage.cfg"

#gzip --force ${BINARIES_DIR}/sdcard.img
