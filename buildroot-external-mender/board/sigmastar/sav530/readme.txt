*************************************************
Mender integration on sigmastar SAV530 EVK board
*************************************************

This file documents the Buildroot integration of Mender for the sigmastar SAV530
EVK board.

Build
=====

First, generate the configuration file,

  ./buildroot-external-mender/board/sigmastar/sav530/gen-defconfig.sh

Configure Buildroot for the SAV530 EVK board:

  make mender_sigmastar_sav530_defconfig

Build  mender components:

  make mender-rebuild

Build sigmastar kernel and modules

   make linux-rebuild

[1]. https://docs.mender.io/architecture/mender-artifacts
[2]. https://docs.mender.io/getting-started/deploy-to-physical-devices
[3]. https://docs.mender.io/architecture/standalone-deployments
