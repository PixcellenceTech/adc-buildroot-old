insmod /config/modules/4.9.84/usb-common.ko
insmod /config/modules/4.9.84/usbcore.ko
insmod /config/modules/4.9.84/ehci-hcd.ko
insmod /config/modules/4.9.84/scsi_mod.ko
insmod /config/modules/4.9.84/usb-storage.ko
insmod /config/modules/4.9.84/cifs.ko
insmod /config/modules/4.9.84/mmc_core.ko
insmod /config/modules/4.9.84/mmc_block.ko
insmod /config/modules/4.9.84/kdrv_sdmmc.ko
insmod /config/modules/4.9.84/fat.ko
insmod /config/modules/4.9.84/msdos.ko
insmod /config/modules/4.9.84/vfat.ko
insmod /config/modules/4.9.84/ntfs.ko
insmod /config/modules/4.9.84/sd_mod.ko
insmod /config/modules/4.9.84/ms_notify.ko
insmod /config/modules/4.9.84/r8152.ko
insmod /config/modules/4.9.84/compat.ko
insmod /config/modules/4.9.84/cfg80211.ko
insmod /config/modules/4.9.84/brcmutil.ko
insmod /config/modules/4.9.84/brcmfmac.ko
#kernel_mod_list
insmod /config/modules/4.9.84/mhal.ko
#misc_mod_list
insmod /config/modules/4.9.84/mi_common.ko
major=`cat /proc/devices | busybox awk "\\$2==\""mi"\" {print \\$1}"\n`
minor=0
insmod /config/modules/4.9.84/mi_sys.ko cmdQBufSize=768 logBufSize=256
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_sys c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_sensor.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_sensor c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_mipitx.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_mipitx c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_ao.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_ao c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_rgn.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_rgn c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_ldc.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_ldc c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_vpe.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_vpe c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_shadow.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_shadow c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_gyro.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_gyro c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_vif.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_vif c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_venc.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_venc c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_divp.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_divp c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_disp.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_disp c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_ipu.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_ipu c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_ai.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_ai c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_panel.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_panel c $major $minor
	let minor++
fi


insmod /config/modules/4.9.84/mi_vdisp.ko
if [ $? -eq 0 ]; then
	busybox mknod /dev/mi_vdisp c $major $minor
	let minor++
fi


#mi module
major=`cat /proc/devices | busybox awk "\\$2==\""mi_poll"\" {print \\$1}"`
busybox mknod /dev/mi_poll c $major 0

#misc_mod_list_late
insmod /config/modules/4.9.84/OS05A20_MIPI.ko chmap=1
mdev -s
