#!/bin/sh

echo "mount ubifs (alarm part)..."
#/usr/sbin/ubiattach /dev/ubi_ctrl -m 10
#> /dev/null 2>&1 
#/usr/sbin/ubimkvol /dev/ubi2 -m -N ubi2_0
#> /dev/null 2>&1 
/bin/mount -t ubifs ubi0:ALARM /alarm
#> /dev/null 2>&1



for i in /etc/init.d/S??* ;do
     # Ignore dangling symlinks (if any).
     [ ! -f "$i" ] && continue

     case "$i" in
        *.sh)
            # Source shell script for speed.
            (
                trap - INT QUIT TSTP
                set start
                . $i
            )
            ;;
        *)
            # No sh extension, so fork subprocess.
            $i start
            ;;
    esac
done

cd /alarm
