#!/bin/sh
export PATH="$PATH:/usr/bin"
#cd /usr/ttlv/bcmdhd/
ifconfig wlan0 up
ifconfig lo up


### DHCP Start ###
bcm_ip="0.0.0.0"
try_cnt=40
conn_thd=20
retry_startT=`expr $try_cnt - $conn_thd`

iot_info_path=/tmp/.iotinfo

echo "0.0.0.0" >> $iot_info_path

#./wl pkt_filter_delete 999
./wl status

#./dhd_priv wl dhcpc_dump 
#cat /tmp/.iotinfo
 ./dhd_priv wl dhcpc_dump > /tmp/.iotinfo

bcm_ip=$(grep "ipaddr" $iot_info_path | awk -F" " '{ print $2 }')


if [ $bcm_ip = "0.0.0.0"  ] || [ "$bcm_ip" == "" ];then
   echo "ip is not ready: $bcm_ip"
   echo "$1"
   echo "$2"
#   ./wl join Vin241

    ./wl join NETGEAR.VK
#   ./wl join VinHome
#   ./wl join ding
   ./dhd_priv wl dhcpc_enable 1
   
fi

echo $bcm_ip

while [ ${try_cnt} -gt 0 ] && [ "$bcm_ip" = "0.0.0.0" ]
do
    ./dhd_priv wl dhcpc_dump > /tmp/.iotinfo
    bcm_ip=$(grep "ipaddr" $iot_info_path | awk -F" " '{ print $2 }')
    try_cnt=`expr $try_cnt - 1`
    echo $try_cnt    
    cat $iot_info_path
    
#    if [ "$ip" == "0.0.0.0" ];then
#    ;
#        if [ $try_cnt -lt $retry_startT ]
#    fi

    usleep 100000
done


if [ $bcm_ip != "0.0.0.0" ] && [ $bcm_ip != "" ];then
ifconfig wlan0 $bcm_ip
echo "Net: Up"
else
echo "Net: Down"
fi

##################
#wl pkt_filter_add 999 1 0 12 0xffff 0x0800

