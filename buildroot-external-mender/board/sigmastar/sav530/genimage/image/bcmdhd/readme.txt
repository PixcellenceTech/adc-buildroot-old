*Broadcom tool:
  dhd_priv
  wl
  iwconfig


*Wifi config:
  config.txt ==> For all wifi module

*Wifi Firmware
  AP6201 FW:
   -clm_bcm43013c1_ag.blob
   -nvram_ap6201bm.txt

  AP6236 FW:
   -fw_bcm43436b0.bin
   -nvram_ap6236.txt

  AP6256 FW:
   -nvram_ap6256.txt
   -fw_bcm43456c5_ag.bin

  AP6201a FW:
   -fw_43438a1_20170915.bin
   -fw_bcm43438a1.bin
   -nvram_ap6212a.txt

  AP6398 FW:
   -fw_bcm4359c0_ag.bin
   -nvram_6398s.txt

*wpa_supplicatn:
  -wpa.conf
  -wpa_supplicant
  -libnl.so.3
  -libnl-genl.so.3
  -bcm_wpa_supplicant.sh

*iot type wifi command:
  -bcm_sdio_speed_test.sh
  -bcm_sleep.sh
  -bcmsleep.sh
  -hw_poweroff.sh
