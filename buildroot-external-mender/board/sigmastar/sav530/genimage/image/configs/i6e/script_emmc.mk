TARGET_SCRIPT:=$(foreach n,$(IMAGE_LIST),$(n)_$(FLASH_TYPE)_$($(n)$(FSTYPE))_script) $(FLASH_TYPE)_config_script

TARGET_FS:=$(filter-out $(patsubst %_fs__,%,$(filter %_fs__, $(foreach n,$(IMAGE_LIST),$(n)_fs_$($(n)$(FSTYPE))_))), $(IMAGE_LIST))

SCRIPTDIR:=$(IMAGEDIR)/scripts

scripts:
	mkdir -p $(SCRIPTDIR)
	$(MAKE) $(TARGET_SCRIPT)
	@echo -e $(foreach n,$(IMAGE_LIST),estar scripts/[[$(n)\\n) >> $(IMAGEDIR)/auto_update.txt
	@echo estar scripts/set_config >> $(IMAGEDIR)/auto_update.txt
	@echo saveenv >> $(IMAGEDIR)/auto_update.txt
	@echo printenv >> $(IMAGEDIR)/auto_update.txt
	@echo reset >> $(IMAGEDIR)/auto_update.txt

#for emmc flash kernel.
kernel_emmc__script:
	@echo "# <- this is for comment / total file size must be less than 4KB" > $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo mmc dev 0 0 >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo tftp $(TFTPDOWNLOADADDR) $(patsubst %_emmc__script,%,$@) >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo mmc write $(TFTPDOWNLOADADDR) 0x800 0x1800 >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo mmc dev 0 1 >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo setenv bootcmd \' mmc read 0x21000000 0x800 0x1800\;bootm 0x21000000\; >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo "% <- this is end of file symbol" >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)

uboot_emmc__script:
	@echo "# <- this is for comment / total file size must be less than 4KB" > $(SCRIPTDIR)/[[uboot
	@echo tftp $(TFTPDOWNLOADADDR) boot/$(notdir $(uboot$(RESOUCE))) >> $(SCRIPTDIR)/[[uboot
	@echo "% <- this is end of file symbol" >> $(SCRIPTDIR)/[[uboot

ipl_emmc__script:
	@echo "# <- this is for comment / total file size must be less than 4KB" > $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo tftp $(TFTPDOWNLOADADDR) boot/$(notdir $(ipl$(RESOUCE))) >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo  "% <- this is end of file symbol" >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)

ipl_cust_emmc__script:
	@echo "# <- this is for comment / total file size must be less than 4KB" > $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo mxp r.info IPL_CUST >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo sf probe 0 >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo sf erase \$${sf_part_start} \$${sf_part_size} >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo tftp $(TFTPDOWNLOADADDR) boot/$(notdir $(ipl_cust$(RESOUCE))) >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo sf write $(TFTPDOWNLOADADDR) \$${sf_part_start} \$${filesize} >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo  "% <- this is end of file symbol" >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)

mxp_emmc__script:
	@echo "# <- this is for comment / total file size must be less than 4KB" > $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo tftp $(TFTPDOWNLOADADDR) boot/MXP_SF.bin >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo  "% <- this is end of file symbol" >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)

iplx_uboot_emmc__script:
	@echo "# <- this is for comment / total file size must be less than 4KB" > $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo mmc dev 0 1 >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo mmc bootbus 0 1 0 0 >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo mmc partconf 0 1 1 1 >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo tftp $(TFTPDOWNLOADADDR) boot/BOOT_PART.bin >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo mmc write $(TFTPDOWNLOADADDR) 0x0 0x200 >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)
	@echo  "% <- this is end of file symbol" >> $(SCRIPTDIR)/[[$(patsubst %_emmc__script,%,$@)

%_emmc_squashfs_script:
	@echo "# <- this is for comment / total file size must be less than 4KB" > $(SCRIPTDIR)/[[$(patsubst %_emmc_squashfs_script,%,$@)
	@echo mmc dev 0 2 >> $(SCRIPTDIR)/[[$(patsubst %_emmc_squashfs_script,%,$@)
	@echo mmc erase 0 0x1000 >> $(SCRIPTDIR)/[[$(patsubst %_emmc_squashfs_script,%,$@)
	@echo tftp $(TFTPDOWNLOADADDR) $(patsubst %_emmc_squashfs_script,%,$@).sqfs >> $(SCRIPTDIR)/[[$(patsubst %_emmc_squashfs_script,%,$@)
	@echo mmc write $(TFTPDOWNLOADADDR) 0 0x1000 >> $(SCRIPTDIR)/[[$(patsubst %_emmc_squashfs_script,%,$@)
	@echo  "% <- this is end of file symbol" >> $(SCRIPTDIR)/[[$(patsubst %_emmc_squashfs_script,%,$@)

%_emmc_ramfs_script:
	@echo "# <- this is for comment / total file size must be less than 4KB" > $(SCRIPTDIR)/[[$(patsubst %_emmc_ramfs_script,%,$@)
	@echo tftp $(TFTPDOWNLOADADDR) $(patsubst %_emmc_ramfs_script,%,$@).ramfs >> $(SCRIPTDIR)/[[$(patsubst %_emmc_ramfs_script,%,$@)
	@echo setenv initrd_high ${INITRAMFSLOADADDR} >> $(SCRIPTDIR)/[[$(patsubst %_emmc_ramfs_script,%,$@)
	@echo setenv initrd_size \$${sf_part_size} >> $(SCRIPTDIR)/[[$(patsubst %_emmc_ramfs_script,%,$@)
	@echo  "% <- this is end of file symbol" >> $(SCRIPTDIR)/[[$(patsubst %_emmc_ramfs_script,%,$@)

%_emmc_jffs2_script:
	@echo "# <- this is for comment / total file size must be less than 4KB" > $(SCRIPTDIR)/[[$(patsubst %_emmc_jffs2_script,%,$@)
	@echo tftp $(TFTPDOWNLOADADDR) $(patsubst %_emmc_jffs2_script,%,$@).jffs2 >> $(SCRIPTDIR)/[[$(patsubst %_emmc_jffs2_script,%,$@)
	@echo  "% <- this is end of file symbol" >> $(SCRIPTDIR)/[[$(patsubst %_emmc_jffs2_script,%,$@)

emmc_config_script:
	@echo "# <- this is for comment / total file size must be less than 4KB" > $(SCRIPTDIR)/set_config
	@echo mmc dev 0 1 >> $(SCRIPTDIR)/set_config
	@echo mmc bootbus 0 1 0 0 >> $(SCRIPTDIR)/set_config
	@echo mmc partconf 0 1 1 0 >> $(SCRIPTDIR)/set_config
	@echo setenv bootargs \' console=ttyS0,115200 root=/dev/mmcblk0boot1 rootwait rootfstype=squashfs ro init=/linuxrc cma=64M >> $(SCRIPTDIR)/set_config
	@echo saveenv >> $(SCRIPTDIR)/set_config
	@echo reset >> $(SCRIPTDIR)/set_config
	@echo  "% <- this is end of file symbol" >> $(SCRIPTDIR)/set_config
