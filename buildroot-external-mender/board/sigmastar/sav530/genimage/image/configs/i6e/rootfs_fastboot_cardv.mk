.PHONY: rootfs

LIB_DIR_PATH:=$(PROJ_ROOT)/release/$(PRODUCT)/$(CHIP)/common/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)
RAMDISK_DIR?=$(OUTPUTDIR)/tmprd
RAMDISK_IMG?=$(ramdisk$(RESOUCE))
RAMFS_CONFIG?=$(OUTPUTDIR)/rootfs/bootconfig

rootfs:
	cd rootfs; tar xf rootfs.tar.gz -C $(OUTPUTDIR)
	tar xf busybox/$(BUSYBOX).tar.gz -C $(OUTPUTDIR)/rootfs

	## ramdisk/other use /linuxrc , ramfs use /init
	if [ "$(rootfs$(FSTYPE))" = "ramfs" ]; then \
		mv $(OUTPUTDIR)/rootfs/linuxrc $(OUTPUTDIR)/rootfs/init ; \
	fi;
	
	tar xf $(LIB_DIR_PATH)/package/$(LIBC).tar.gz -C $(OUTPUTDIR)/rootfs/lib
	cp -d $(LIB_DIR_PATH)/mi_libs/dynamic/* $(OUTPUTDIR)/rootfs/lib/
	cp -d $(LIB_DIR_PATH)/ex_libs/dynamic/* $(OUTPUTDIR)/rootfs/lib/

	mkdir -p $(miservice$(RESOUCE))
	mkdir -p $(miservice$(RESOUCE))/bin
	mkdir -p $(OUTPUTDIR)/customer
	mkdir -p $(RAMFS_CONFIG)
	mkdir -p $(RAMFS_CONFIG)/bin

	#cp -rf $(PROJ_ROOT)/board/ini/* $(OUTPUTDIR)/customer
	cp -rf $(PROJ_ROOT)/board/$(CHIP)/$(BOARD_NAME)/config/* $(RAMFS_CONFIG)
	cp -rf $(LIB_DIR_PATH)/bin/cardv/* $(RAMFS_CONFIG)/bin
	# rm -rf $(miservice$(RESOUCE))/bin/cardv
	# cp -ar $(LIB_DIR_PATH)/bin/audio $(OUTPUTDIR)/customer
	# cp -ar $(LIB_DIR_PATH)/bin/wifi $(OUTPUTDIR)/customer

	cp -vf $(PROJ_ROOT)/board/$(CHIP)/mmap/$(MMAP) $(RAMFS_CONFIG)/mmap.ini
	cp -rvf $(LIB_DIR_PATH)/bin/config_tool/* $(RAMFS_CONFIG)
	cd $(RAMFS_CONFIG); chmod +x config_tool; ln -sf config_tool dump_config; ln -sf config_tool dump_mmap
	mkdir -p $(RAMFS_CONFIG)/iqfile/
	cp -rf $(PROJ_ROOT)/board/$(CHIP)/iqfile/isp_api.xml $(RAMFS_CONFIG)/iqfile/ -vf
	if [ "$(IQ0)" != "" ]; then \
		cp -rf $(PROJ_ROOT)/board/$(CHIP)/iqfile/$(IQ0) $(RAMFS_CONFIG)/iqfile/ -vf; \
		cd $(RAMFS_CONFIG)/iqfile; chmod +x $(shell echo $(IQ0) | awk -F'/' '{print $$NF}'); ln -sf $(shell echo $(IQ0) | awk -F'/' '{print $$NF}') iqfile0.bin; cd -; \
	fi;
	if [ "$(IQ1)" != "" ]; then \
		cp -rf $(PROJ_ROOT)/board/$(CHIP)/iqfile/$(IQ1) $(RAMFS_CONFIG)/iqfile/ -vf; \
		cd $(RAMFS_CONFIG)/iqfile; chmod +x $(shell echo $(IQ1) | awk -F'/' '{print $$NF}'); ln -sf $(shell echo $(IQ1) | awk -F'/' '{print $$NF}') iqfile1.bin; cd -; \
	fi;
	if [ "$(IQ2)" != "" ]; then \
		cp -rf $(PROJ_ROOT)/board/$(CHIP)/iqfile/$(IQ2) $(RAMFS_CONFIG)/iqfile/ -vf; \
		cd $(RAMFS_CONFIG)/iqfile; chmod +x $(shell echo $(IQ2) | awk -F'/' '{print $$NF}'); ln -sf $(shell echo $(IQ2) | awk -F'/' '{print $$NF}') iqfile2.bin; cd -; \
	fi;
	if [ "$(IQ3)" != "" ]; then \
		cp -rf $(PROJ_ROOT)/board/$(CHIP)/iqfile/$(IQ3) $(RAMFS_CONFIG)/iqfile/ -vf; \
		cd $(RAMFS_CONFIG)/iqfile; chmod +x $(shell echo $(IQ3) | awk -F'/' '{print $$NF}'); ln -sf $(shell echo $(IQ3) | awk -F'/' '{print $$NF}') iqfile3.bin; cd -; \
	fi;
	if [ -d $(PROJ_ROOT)/board/$(CHIP)/venc_fw ]; then \
		cp -rf $(PROJ_ROOT)/board/$(CHIP)/venc_fw $(RAMFS_CONFIG); \
	fi;

	#if [ -d $(PROJ_ROOT)/board/$(CHIP)/dla_file ]; then \
	#	mkdir $(RAMFS_CONFIG)/dla; \
	#	cp $(PROJ_ROOT)/board/$(CHIP)/dla_file/ipu_firmware.bin $(RAMFS_CONFIG)/dla; \
	#fi;

	mkdir -p $(OUTPUTDIR)/rootfs/config
	mkdir -p $(OUTPUTDIR)/rootfs/mnt/mmc
	mkdir -p $(OUTPUTDIR)/rootfs/mnt/nfs
	cp -rf etc/* $(OUTPUTDIR)/rootfs/etc

	if [ $(BENCH) = "yes" ]; then \
		cp -rf /home/edie.chen/bench $(RAMFS_CONFIG) ; \
		cp $(RAMFS_CONFIG)/bench/demo.bash $(RAMFS_CONFIG)/bench.sh ; \
		chmod 755 $(RAMFS_CONFIG)/bench.sh ; \
	fi;

	if [ "$(PHY_TEST)" = "yes" ]; then \
		mkdir $(RAMFS_CONFIG)/sata_phy ; \
		cp $(LIB_DIR_PATH)/bin/sata_phy/* $(RAMFS_CONFIG)/sata_phy ; \
	fi;

	#clean profile
	echo '#!/bin/sh' > ${OUTPUTDIR}/rootfs/etc/profile
	echo 'echo 20 > sys/class/mstar/msys/booting_time' >> ${OUTPUTDIR}/rootfs/etc/profile
	echo export PATH=/bin:/sbin:/usr/bin:/usr/sbin >> ${OUTPUTDIR}/rootfs/etc/profile
	echo export LD_LIBRARY_PATH=/lib >> ${OUTPUTDIR}/rootfs/etc/profile
	echo ulimit -c unlimited >> ${OUTPUTDIR}/rootfs/etc/profile
	
	if [ $(FLASH_TYPE) = "nor" ]; then \
		if [ $(FLASH_SIZE) != "8M" ]; then \
			cp -ar $(LIB_DIR_PATH)/bin/audio $(OUTPUTDIR)/customer ; \
			if [ "$(WIFI_MODULE)" != "" ]; then \
				echo export WIFI_MODULE=$(WIFI_MODULE) >> ${OUTPUTDIR}/rootfs/etc/profile; \
			fi; \
		fi; \
	else \
		cp -rf $(LIB_DIR_PATH)/bin/mixer/* $(OUTPUTDIR)/customer/ ; \
		echo export LD_LIBRARY_PATH=\$$LD_LIBRARY_PATH:/customer >> ${OUTPUTDIR}/rootfs/etc/profile; \
		ln -sf ./mixer/font $(OUTPUTDIR)/customer/font ; \
		cp -rf $(LIB_DIR_PATH)/bin/mi_demo/ $(OUTPUTDIR)/customer/ ; \
	fi;

	mkdir -p $(OUTPUTDIR)/rootfs/lib/modules/
	mkdir -p $(RAMFS_CONFIG)/modules/$(KERNEL_VERSION)
	mkdir -p $(miservice$(RESOUCE))/modules/$(KERNEL_VERSION)
	
	#clean default rcS
	echo '#!/bin/sh' > ${OUTPUTDIR}/rootfs/etc/init.d/rcS
	echo mount -a >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS
	echo 'echo /sbin/mdev >> /proc/sys/kernel/hotplug' >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo /sbin/sysctl -p -n >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	
	
	touch ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo mice 0:0 0660 =input/ >> ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo mouse.* 0:0 0660 =input/ >> ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo event.* 0:0 0660 =input/ >> ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo pcm.* 0:0 0660 =snd/ >> ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo control.* 0:0 0660 =snd/ >> ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo timer 0:0 0660 =snd/ >> ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo '$$DEVNAME=bus/usb/([0-9]+)/([0-9]+) 0:0 0660 =bus/usb/%1/%2' >> ${OUTPUTDIR}/rootfs/etc/mdev.conf

	echo export PATH=\$$PATH:/config:/bootconfig >> ${OUTPUTDIR}/rootfs/etc/profile
	echo export TERMINFO=/config/terminfo >> ${OUTPUTDIR}/rootfs/etc/profile
	sed -i '/^mount.*/d' $(OUTPUTDIR)/rootfs/etc/profile
#	echo mkdir -p /dev/pts >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS
#	echo mount -t sysfs none /sys >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo mount -t tmpfs mdev /dev >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
#	echo mount -t debugfs none /sys/kernel/debug/ >>  $(OUTPUTDIR)/rootfs/etc/init.d/rcS

	cp -rvf $(PROJ_ROOT)/tools/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/fw_printenv/* $(OUTPUTDIR)/rootfs/etc/
	echo "$(ENV_CFG)" > $(OUTPUTDIR)/rootfs/etc/fw_env.config
	cd $(OUTPUTDIR)/rootfs/etc/;ln -sf fw_printenv fw_setenv

	echo mkdir -p /var/lock >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS
	if [ "$(FLASH_TYPE)"x = "nor"x  ]; then \
		echo mdev -s >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS ;\
	fi;

#	echo -e $(foreach block, $(USR_MOUNT_BLOCKS), "mount -t $($(block)$(FSTYPE)) $($(block)$(MOUNTPT)) $($(block)$(MOUNTTG))\n") >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
#	echo 'mount -t squashfs /dev/mtdblock3 /config' >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS
# 	mount -t jffs2 mtd:customer /customer
	
	-chmod 755 $(LIB_DIR_PATH)/bin/debug/*
	cp -rf $(LIB_DIR_PATH)/bin/debug/* $(OUTPUTDIR)/customer/
	
	if [ -f "$(OUTPUTDIR)/customer/iperf" ]; then \
		rm -rf $(OUTPUTDIR)/customer/iperf; \
		rm -rf $(OUTPUTDIR)/customer/perf; \
	fi;
	
	#add:  remove sshd in nor flash default
	if [[ "$(FLASH_TYPE)"x = "nor"x ]] && [[ -d "$(OUTPUTDIR)/customer/ssh" ]]; then \
		rm -rf $(OUTPUTDIR)/customer/ssh; \
	fi;
	#end add

	if [ -f "$(RAMFS_CONFIG)/demo.sh" ]; then \
		rm  $(RAMFS_CONFIG)/demo.sh; \
	fi;
	touch $(RAMFS_CONFIG)/demo.sh
	chmod 755 $(RAMFS_CONFIG)/demo.sh


	# creat insmod mhal/MI/sensor ko scrpit
	if [ "$(DUAL_OS)" != "on" ]; then \
		if [ -f "$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/misc_mod_list" ]; then \
			cat $(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/misc_mod_list | sed 's#\(.*\).ko#insmod /bootconfig/modules/$(KERNEL_VERSION)/\1.ko#' >> $(RAMFS_CONFIG)/demo.sh; \
			cat $(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/misc_mod_list | sed 's#\(.*\).ko\(.*\)#$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/\1.ko#' | xargs -i cp -rvf {} $(RAMFS_CONFIG)/modules/$(KERNEL_VERSION); \
			echo "#misc_mod_list" >> $(RAMFS_CONFIG)/demo.sh; \
		fi; \
		if [ -f "$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/.mods_depend" ]; then \
			cat $(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/.mods_depend | sed '2,20s#\(.*\)#insmod /bootconfig/modules/$(KERNEL_VERSION)/\1.ko#' >> $(RAMFS_CONFIG)/demo.sh; \
			cat $(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/.mods_depend | sed 's#\(.*\)#$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/\1.ko#' | xargs -i cp -rvf {} $(RAMFS_CONFIG)/modules/$(KERNEL_VERSION); \
			echo "#mi module" >> $(RAMFS_CONFIG)/demo.sh; \
		fi; \
	fi;

	if [ "$(DUAL_OS)" != "on" ]; then \
		if [ "$(SENSOR_LIST)" != "" ]; then \
			cp -rvf $(foreach n,$(SENSOR_LIST),$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/$(n)) $(RAMFS_CONFIG)/modules/$(KERNEL_VERSION); \
		fi; \
		if [ "$(SENSOR0)" != "" ]; then \
			echo insmod /bootconfig/modules/$(KERNEL_VERSION)/$(SENSOR0) $(SENSOR0_OPT) >> $(RAMFS_CONFIG)/demo.sh; \
		fi; \
		if [ "$(SENSOR1)" != "" ]; then \
			echo insmod /bootconfig/modules/$(KERNEL_VERSION)/$(SENSOR1) $(SENSOR1_OPT) >> $(RAMFS_CONFIG)/demo.sh; \
		fi; \
		if [ "$(SENSOR2)" != "" ]; then \
			echo insmod /bootconfig/modules/$(KERNEL_VERSION)/$(SENSOR2) $(SENSOR2_OPT) >> $(RAMFS_CONFIG)/demo.sh; \
		fi;	\
		sed -i 's/mi_sys.ko/mi_sys.ko cmdQBufSize=768 logBufSize=256/g' $(RAMFS_CONFIG)/demo.sh; \
		sed -i 's/mi_venc.ko/mi_venc.ko fw_path="\/bootconfig\/venc_fw\/chagall.bin"/g' $(RAMFS_CONFIG)/demo.sh; \
		sed -i '/mi_iqserver.ko/,+4d' $(RAMFS_CONFIG)/demo.sh;\
		sed -i '/mi_isp.ko/,+4d' $(RAMFS_CONFIG)/demo.sh;\
		sed -i 's/mi_common/insmod \/bootconfig\/modules\/$(KERNEL_VERSION)\/mi_common.ko/g' $(RAMFS_CONFIG)/demo.sh; \
		sed -i '/#mi module/a	major=`cat /proc/devices | busybox awk "\\\\$$2==\\""mi_poll"\\" {print \\\\$$1}"`\nbusybox mknod \/dev\/mi_poll c $$major 0' $(RAMFS_CONFIG)/demo.sh; \
	fi;

	echo 'echo 32 > sys/class/mstar/msys/booting_time' >> $(RAMFS_CONFIG)/demo.sh
	# run cardv program.
	echo "echo /bootconfig/iqfile > /sys/devices/virtual/mstar/ispmid0/isproot" >> $(RAMFS_CONFIG)/demo.sh
	echo "cardv -o -N 1 &" >> $(RAMFS_CONFIG)/demo.sh
	echo "wait_rec.sh" >> $(RAMFS_CONFIG)/demo.sh
	echo 'echo 33 > sys/class/mstar/msys/booting_time' >> $(RAMFS_CONFIG)/demo.sh

	echo -e $(foreach block, $(USR_MOUNT_BLOCKS), "mount -t $($(block)$(FSTYPE)) $($(block)$(MOUNTPT)) $($(block)$(MOUNTTG))\n") >> $(RAMFS_CONFIG)/demo.sh
	echo 'echo 34 > sys/class/mstar/msys/booting_time' >> $(RAMFS_CONFIG)/demo.sh

	# insmod fbdev_video.ko 
	if [ "$(DUAL_OS)" != "on" ]; then \
	if [ -f "$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/misc_mod_list_late" ]; then \
		cat $(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/misc_mod_list_late | sed 's#\(.*\).ko#insmod /bootconfig/modules/$(KERNEL_VERSION)/\1.ko#' >> $(RAMFS_CONFIG)/demo.sh; \
		cat $(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/misc_mod_list_late | sed 's#\(.*\).ko\(.*\)#$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/\1.ko#' | xargs -i cp -rvf {} $(RAMFS_CONFIG)/modules/$(KERNEL_VERSION); \
		echo "#misc_mod_list_late" >> $(RAMFS_CONFIG)/demo.sh; \
		cp -ar $(RAMFS_CONFIG)/fbdev.ini $(miservice$(RESOUCE)); \
		rm -rf $(RAMFS_CONFIG)/fbdev.ini; \
	fi; \
	fi;
	
	# creat insmod kernel ko scrpit
	if [ -f "$(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/kernel_mod_list" ]; then \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/kernel_mod_list | sed 's#\(.*\).ko#insmod /config/modules/$(KERNEL_VERSION)/\1.ko#' >> $(RAMFS_CONFIG)/demo.sh; \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/kernel_mod_list | sed 's#\(.*\).ko\(.*\)#$(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/\1.ko#' | xargs -i cp -rvf {} $(miservice$(RESOUCE))/modules/$(KERNEL_VERSION); \
		echo "#kernel_mod_list" >> $(RAMFS_CONFIG)/demo.sh; \
	fi;

	if [ -f "$(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/kernel_mod_list_late" ]; then \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/kernel_mod_list_late | sed 's#\(.*\).ko#insmod /config/modules/$(KERNEL_VERSION)/\1.ko#' >> $(RAMFS_CONFIG)/demo.sh; \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/kernel_mod_list_late | sed 's#\(.*\).ko\(.*\)#$(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/\1.ko#' | xargs -i cp -rvf {} $(miservice$(RESOUCE))/modules/$(KERNEL_VERSION); \
		echo "#kernel_mod_list_late" >> $(RAMFS_CONFIG)/demo.sh; \
	fi;

	# run cardv UI program.
	echo "zkgui &" >> $(RAMFS_CONFIG)/demo.sh

	if [ $(PHY_TEST) = "yes" ]; then \
		echo -e "\033[41;33;5m !!! Replace "mdrv-sata-host.ko" with "sata_bench_test.ko" !!!\033[0m" ; \
		sed '/mdrv-sata-host/d' $(RAMFS_CONFIG)/demo.sh > $(OUTPUTDIR)/customer/demotemp.sh ; \
		echo insmod /config/sata_phy/sata_bench_test.ko >> $(OUTPUTDIR)/customer/demotemp.sh ; \
		cp $(OUTPUTDIR)/customer/demotemp.sh $(RAMFS_CONFIG)/demo.sh ; \
		rm $(OUTPUTDIR)/customer/demotemp.sh ; \
	fi;

	# Enable MIU protect on CMDQ buffer as default (While List: CPU)
	# [I5] The 1st 1MB of MIU is not for CMDQ buffer
#	echo 'echo set_miu_block3_status 0 0x70 0 0x100000 1 > /proc/mi_modules/mi_sys_mma/miu_protect' >> $(RAMFS_CONFIG)/demo.sh

#	echo mount -t jffs2 /dev/mtdblock3 /config >> $(OUTPUTDIR)/rootfs/etc/profile
	ln -fs /bootconfig/modules/$(KERNEL_VERSION) $(OUTPUTDIR)/rootfs/lib/modules/
	find $(RAMFS_CONFIG)/modules/$(KERNEL_VERSION) -name "*.ko" | xargs $(TOOLCHAIN_REL)strip  --strip-unneeded;
	find $(miservice$(RESOUCE))/modules/$(KERNEL_VERSION) -name "*.ko" | xargs $(TOOLCHAIN_REL)strip  --strip-unneeded;
	find $(OUTPUTDIR)/rootfs/lib/ -name "*.so" | xargs $(TOOLCHAIN_REL)strip  --strip-unneeded;
	$(TOOLCHAIN_REL)strip  --strip-unneeded $(OUTPUTDIR)/rootfs/bin/*
	$(TOOLCHAIN_REL)strip  --strip-unneeded $(OUTPUTDIR)/rootfs/lib/*.so

	echo mkdir -p /dev/pts >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo mount -t devpts devpts /dev/pts >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "busybox telnetd&" >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	
	# set PATH & LD_LIBRARY_PATH $ env
	echo export TSLIB_TSDEVICE=/dev/input/event1 >> ${OUTPUTDIR}/rootfs/etc/profile
	echo export KEY_DEVICE=/dev/input/event0 >> ${OUTPUTDIR}/rootfs/etc/profile
	echo export PATH=\$$PATH:/bootconfig/bin:/customer/UI/bin:/customer/wifi >> ${OUTPUTDIR}/rootfs/etc/profile
	echo export LD_LIBRARY_PATH=\$$LD_LIBRARY_PATH:/customer/UI/lib:/customer/wifi/lib >> ${OUTPUTDIR}/rootfs/etc/profile

	echo "if [ -e /bootconfig/demo.sh ]; then" >> $(OUTPUTDIR)/rootfs/etc/profile
	echo "    /bootconfig/demo.sh" >> $(OUTPUTDIR)/rootfs/etc/profile
	echo "fi;" >> $(OUTPUTDIR)/rootfs/etc/profile
	
	if [ $(BENCH) = "yes" ]; then \
		echo ./config/bench.sh >> $(RAMFS_CONFIG)/demo.sh ; \
	fi;
	#add sshd, default password 1234
	if [[ "$(FLASH_TYPE)"x = "spinand"x ]]; then \
		if [[ $(TOOLCHAIN_VERSION) = "9.1.0" ]] || [[ $(TOOLCHAIN_VERSION) = "8.2.1" ]]; then \
			echo "root:5fXKAeftHX95A:0:0:Linux User,,,:/home/root:/bin/sh" > $(OUTPUTDIR)/rootfs/etc/passwd; \
			echo "sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin" >> $(OUTPUTDIR)/rootfs/etc/passwd; \
			echo "export LD_LIBRARY_PATH=\$$LD_LIBRARY_PATH:/customer/ssh/lib" >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS; \
			echo "mkdir /var/empty" >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS; \
			echo "/customer/ssh/sbin/sshd -f /customer/ssh/etc/sshd_config" >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS; \
			echo "export LD_LIBRARY_PATH=\$$LD_LIBRARY_PATH:/customer/ssh/lib" >> ${OUTPUTDIR}/rootfs/etc/profile; \
		fi; \
	fi;
	#end add

	mkdir -p $(OUTPUTDIR)/vendor
	mkdir -p $(OUTPUTDIR)/customer
	mkdir -p $(OUTPUTDIR)/rootfs/vendor
	mkdir -p $(OUTPUTDIR)/rootfs/customer
	mkdir -p $(OUTPUTDIR)/bootconfig
	mkdir -p $(OUTPUTDIR)/rootfs/bootconfig

	# create interface wlan
	mkdir -p $(OUTPUTDIR)/customer/wifi; \
	cp -rf $(LIB_DIR_PATH)/bin/wifi/* $(OUTPUTDIR)/customer/wifi; \
	find $(OUTPUTDIR)/customer/wifi/lib/ -type f ! -name cfg80211.ko | grep -E "*.ko" | xargs -i rm -rf {}; \
	cp -rf $(LIB_DIR_PATH)/bin/wifi/lib/$(WIFI_MODULE).ko $(OUTPUTDIR)/customer/wifi/lib; \
	chmod 755 $(OUTPUTDIR)/customer/wifi -R; \
	$(TOOLCHAIN_REL)strip  --strip-unneeded $(OUTPUTDIR)/customer/wifi/*; \
	$(TOOLCHAIN_REL)strip  --strip-unneeded $(OUTPUTDIR)/customer/wifi/lib/*; \
	find $(OUTPUTDIR)/customer/wifi -type f | grep -E "*.html|*.jpg|*.ico|default.script|iwconfig|iwgetid|iwpriv|ledctrl.sh" | xargs -i rm -rf {}

	echo "if [ -e /customer/wifi/rcInsDriver.sh ]; then" >> $(RAMFS_CONFIG)/demo.sh
	echo "	/customer/wifi/rcInsDriver.sh" >> $(RAMFS_CONFIG)/demo.sh
	echo "fi;" >> $(RAMFS_CONFIG)/demo.sh
	#echo "if [ -e /config/bin/key.sh ]; then" >> $(RAMFS_CONFIG)/demo.sh
	#echo "	/config/bin/key.sh &" >> $(RAMFS_CONFIG)/demo.sh
	#echo "fi;" >> $(RAMFS_CONFIG)/demo.sh
	
	#echo "if [ -e /config/bin/ledctrl.sh ]; then" >> $(RAMFS_CONFIG)/demo.sh
	#echo "    /config/bin/ledctrl.sh &" >> $(RAMFS_CONFIG)/demo.sh
	#echo "fi;" >> $(RAMFS_CONFIG)/demo.sh
	
	#echo "if [ -e /config/bin/ACCdet.sh ]; then" >> $(RAMFS_CONFIG)/demo.sh
	#echo "	/config/bin/ACCdet.sh &" >> $(RAMFS_CONFIG)/demo.sh
	#echo "fi;" >> $(RAMFS_CONFIG)/demo.sh
	#find $(OUTPUTDIR)/rootfs/config/bin -type f | grep -E "ledctrl.sh|key.sh|ACCdet.sh" | xargs -i rm -rf {}


	#echo "if [ -e /config/ota/otaCrond.sh ]; then" >> $(RAMFS_CONFIG)/demo.sh
	#echo "	/customer/cardv_src/ota/otaCrond.sh" >> $(RAMFS_CONFIG)/demo.sh
	#echo "fi;" >> $(RAMFS_CONFIG)/demo.sh
	
	#echo "if [ -e /customer/wifi/run_ota.sh ]; then" >> $(RAMFS_CONFIG)/demo.sh
	#echo "	/customer/wifi/run_ota.sh" >> $(RAMFS_CONFIG)/demo.sh
	#echo "fi;" >> $(RAMFS_CONFIG)/demo.sh
	
	#echo 'park_status=`cat /sys/devices/virtual/input/input0/int2_start_status`' >> $(RAMFS_CONFIG)/demo.sh
	#echo 'if [ $$park_status = "1" ]; then' >> $(RAMFS_CONFIG)/demo.sh
	#echo '	echo "rec 2" > /tmp/cardv_fifo' >> $(RAMFS_CONFIG)/demo.sh
	#echo "fi" >> $(RAMFS_CONFIG)/demo.sh
	#echo "usleep 50000" >> $(RAMFS_CONFIG)/demo.sh

	#echo "echo rtsp 1 > /tmp/cardv_fifo" >> $(RAMFS_CONFIG)/demo.sh
	#echo "usleep 50000" >> $(RAMFS_CONFIG)/demo.sh
	#echo "echo speech 1 > /tmp/cardv_fifo" >> $(RAMFS_CONFIG)/demo.sh

	# UI related binrary & library & resource & EasyUI.cfg
	mkdir -p $(OUTPUTDIR)/customer/UI/bin; \
	mkdir -p $(OUTPUTDIR)/customer/UI/lib; \
	mkdir -p $(OUTPUTDIR)/customer/UI/res; \
	mkdir -p $(OUTPUTDIR)/customer/etc; \
	cp -drf $(LIB_DIR_PATH)/bin/UI/bin/* $(OUTPUTDIR)/customer/UI/bin; \
	cp -drf $(LIB_DIR_PATH)/bin/UI/lib/* $(OUTPUTDIR)/customer/UI/lib; \
	$(TOOLCHAIN_REL)strip  --strip-unneeded $(OUTPUTDIR)/customer/UI/bin/*; \
	$(TOOLCHAIN_REL)strip  --strip-unneeded $(RAMFS_CONFIG)/bin/*; \
	$(TOOLCHAIN_REL)strip  --strip-unneeded $(OUTPUTDIR)/customer/UI/lib/*; \
	cp -drf $(LIB_DIR_PATH)/bin/UI/res/* $(OUTPUTDIR)/customer/UI/res; \
	cp -drf $(LIB_DIR_PATH)/bin/UI/etc/* $(OUTPUTDIR)/customer/etc; \

	if [ $(ramdisk$(RESOUCE)) != "" ]; then \
		rm -rf $(RAMDISK_DIR); \
		mkdir -p $(RAMDISK_DIR); \
		cd $(RAMDISK_DIR); cpio -i -F $(RAMDISK_IMG); \
		cp -R $(OUTPUTDIR)/tmprd/linuxrc $(OUTPUTDIR)/tmprd/init; \
		rm -rf $(OUTPUTDIR)/tmprd/usr/lib/modules/*; \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/ramdisk_rc1 | sed 's#\(.*\).ko\(.*\)#$(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/\1.ko#' | xargs -i cp -rvf {} $(OUTPUTDIR)/tmprd/usr/lib/modules; \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/ramdisk_rc2 | sed 's#\(.*\).ko\(.*\)#$(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/\1.ko#' | xargs -i cp -rvf {} $(OUTPUTDIR)/tmprd/usr/lib/modules; \
		$(PREFIX)strip --strip-unneeded $(OUTPUTDIR)/tmprd/usr/lib/modules/*; \
		cd $(RAMDISK_DIR); find | cpio -o -H newc -O $(RAMDISK_IMG); \
		echo "#!/bin/sh" > $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "echo Console done" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mount -a" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/ramdisk_rc2 | sed 's#\(.*\).ko#insmod /usr/lib/modules/\1.ko#' >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "ubiattach /dev/ubi_ctrl -m $(UBI_AT_MTD)" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mount -t ubifs ubi0:rootfs /usr" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mount -t ubifs ubi0:miservice /config" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mount -t ubifs ubi0:customer /customer" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mkdir -p /dev/shm" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mkdir -p /dev/pts" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mount -t devpts for_telnetd /dev/pts" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "/usr/usr/sbin/telnetd -l /usr/bin/ash" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mount --bind /usr/bin/sh /bin/sh" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "#!/bin/sh" > $(OUTPUTDIR)/tmprd/etc/profile; \
		echo "export PATH=/usr/bin:/usr/sbin:/bin:/sbin:/usr/usr/bin:/usr/usr/sbin" >> $(OUTPUTDIR)/tmprd/etc/profile; \
		echo "/usr/bin/busybox sh /miservice/config/demo.sh" >> $(OUTPUTDIR)/tmprd/etc/profile; \
		echo export TERMINFO=/config/terminfo >> ${OUTPUTDIR}/tmprd/etc/profile; \
		cd $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION); \
		./scripts/gen_initramfs_list.sh -o $(OUTPUTDIR)/initramfs.gz -u 0 -g 0 $(OUTPUTDIR)/tmprd/; \
	fi;
