.PHONY: rootfs

LIB_DIR_PATH:=$(PROJ_ROOT)/release/$(PRODUCT)/$(CHIP)/common/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)
RAMDISK_DIR?=$(OUTPUTDIR)/tmprd
RAMDISK_IMG?=$(ramdisk$(RESOUCE))

rootfs:
	rm -rf  $(OUTPUTDIR)/rootfs
	mkdir -p $(OUTPUTDIR)/rootfs
	tar xf $(PROJ_ROOT)/board/i6e/boot/spinand/rootfs/rootfs.tar -C $(OUTPUTDIR)/rootfs
	tar -czvf  $(OUTPUTDIR)/rootfs/firmware/etc.tar.gz -C $(OUTPUTDIR)/rootfs/etc/  .
	cp $(PROJ_ROOT)/board/i6e/boot/spinand/kernel/uImage.xz  $(OUTPUTDIR)/rootfs/boot/kernel
	#tar xf busybox/$(BUSYBOX).tar.gz -C $(OUTPUTDIR)/rootfs
	#tar xf $(LIB_DIR_PATH)/package/$(LIBC).tar.gz -C $(OUTPUTDIR)/rootfs/lib
	cp $(LIB_DIR_PATH)/mi_libs/dynamic/* $(OUTPUTDIR)/rootfs/lib/
	cp $(LIB_DIR_PATH)/ex_libs/dynamic/* $(OUTPUTDIR)/rootfs/lib/

	mkdir -p $(miservice$(RESOUCE))
	mkdir -p $(OUTPUTDIR)/rootfs/customer
	cp -rf $(PROJ_ROOT)/board/ini/* $(OUTPUTDIR)/rootfs/customer
	cp -rf $(PROJ_ROOT)/board/$(CHIP)/$(BOARD_NAME)/config/* $(miservice$(RESOUCE))
	cp -vf $(PROJ_ROOT)/board/$(CHIP)/mmap/$(MMAP) $(miservice$(RESOUCE))/mmap.ini
	cp -rvf $(LIB_DIR_PATH)/bin/config_tool/* $(miservice$(RESOUCE))
	cd $(miservice$(RESOUCE)); chmod +x config_tool; ln -sf config_tool dump_config; ln -sf config_tool dump_mmap
	mkdir -p $(miservice$(RESOUCE))/iqfile/
	cp -rf $(PROJ_ROOT)/board/$(CHIP)/iqfile/isp_api.xml $(miservice$(RESOUCE))/iqfile/ -vf
	if [ "$(IQ0)" != "" ]; then \
		cp -rf $(PROJ_ROOT)/board/$(CHIP)/iqfile/$(IQ0) $(miservice$(RESOUCE))/iqfile/ -vf; \
		cd $(miservice$(RESOUCE))/iqfile; chmod +x $(shell echo $(IQ0) | awk -F'/' '{print $$NF}'); ln -sf $(shell echo $(IQ0) | awk -F'/' '{print $$NF}') iqfile0.bin; cd -; \
	fi;
	if [ "$(IQ1)" != "" ]; then \
		cp -rf $(PROJ_ROOT)/board/$(CHIP)/iqfile/$(IQ1) $(miservice$(RESOUCE))/iqfile/ -vf; \
		cd $(miservice$(RESOUCE))/iqfile; chmod +x $(shell echo $(IQ1) | awk -F'/' '{print $$NF}'); ln -sf $(shell echo $(IQ1) | awk -F'/' '{print $$NF}') iqfile1.bin; cd -; \
	fi;
	if [ "$(IQ2)" != "" ]; then \
		cp -rf $(PROJ_ROOT)/board/$(CHIP)/iqfile/$(IQ2) $(miservice$(RESOUCE))/iqfile/ -vf; \
		cd $(miservice$(RESOUCE))/iqfile; chmod +x $(shell echo $(IQ2) | awk -F'/' '{print $$NF}'); ln -sf $(shell echo $(IQ2) | awk -F'/' '{print $$NF}') iqfile2.bin; cd -; \
	fi;
	if [ "$(IQ3)" != "" ]; then \
		cp -rf $(PROJ_ROOT)/board/$(CHIP)/iqfile/$(IQ3) $(miservice$(RESOUCE))/iqfile/ -vf; \
		cd $(miservice$(RESOUCE))/iqfile; chmod +x $(shell echo $(IQ3) | awk -F'/' '{print $$NF}'); ln -sf $(shell echo $(IQ3) | awk -F'/' '{print $$NF}') iqfile3.bin; cd -; \
	fi;
	if [ -d $(PROJ_ROOT)/board/$(CHIP)/venc_fw ]; then \
		cp -rf $(PROJ_ROOT)/board/$(CHIP)/venc_fw $(miservice$(RESOUCE)); \
	fi;
	if [ -d $(PROJ_ROOT)/board/$(CHIP)/dla_file ]; then \
		mkdir $(miservice$(RESOUCE))/dla; \
		cp $(PROJ_ROOT)/board/$(CHIP)/dla_file/ipu_firmware.bin $(miservice$(RESOUCE))/dla; \
	fi;

	mkdir -p $(OUTPUTDIR)/rootfs/config
	cp -rf etc/* $(OUTPUTDIR)/rootfs/etc

	if [ $(BENCH) = "yes" ]; then \
		cp -rf /home/edie.chen/bench $(miservice$(RESOUCE)) ; \
		cp $(miservice$(RESOUCE))/bench/demo.bash $(miservice$(RESOUCE))/bench.sh ; \
		chmod 755 $(miservice$(RESOUCE))/bench.sh ; \
	fi;

	if [ "$(PHY_TEST)" = "yes" ]; then \
		mkdir $(miservice$(RESOUCE))/sata_phy ; \
		cp $(LIB_DIR_PATH)/bin/sata_phy/* $(miservice$(RESOUCE))/sata_phy ; \
	fi;


	if [ $(FLASH_TYPE) = "nor" ]; then \
		if [ $(FLASH_SIZE) != "8M" ]; then \
			cp -rf $(LIB_DIR_PATH)/bin/mixer/* $(OUTPUTDIR)/rootfs/customer/ ; \
			echo export LD_LIBRARY_PATH=\$$LD_LIBRARY_PATH:/customer >> ${OUTPUTDIR}/rootfs/etc/profile; \
			ln -sf ./mixer/font $(OUTPUTDIR)/rootfs/customer/font ; \
			if [ -f "$(LIB_DIR_PATH)/bin/mi_demo/prog_rtsp" ]; then \
				cp -rf $(LIB_DIR_PATH)/bin/mi_demo/prog_rtsp $(OUTPUTDIR)/rootfs/customer/; \
			fi;	\
		fi; \
	else \
		cp -rf $(LIB_DIR_PATH)/bin/mixer/* $(OUTPUTDIR)/rootfs/customer/ ; \
		echo export LD_LIBRARY_PATH=\$$LD_LIBRARY_PATH:/customer >> ${OUTPUTDIR}/rootfs/etc/profile; \
		ln -sf ./mixer/font $(OUTPUTDIR)/rootfs/customer/font ; \
		cp -rf $(LIB_DIR_PATH)/bin/mi_demo/ $(OUTPUTDIR)/rootfs/customer/ ; \
	fi;

	if [ "$(DUAL_OS)" = "on" ]; then \
		cp $(PROJ_ROOT)/board/$(CHIP)/rtos/PreloadSetting.txt $(OUTPUTDIR)/customer/ ; \
	fi;

	mkdir -p $(OUTPUTDIR)/rootfs/lib/modules/
	mkdir -p $(miservice$(RESOUCE))/modules/$(KERNEL_VERSION)

	touch ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo mice 0:0 0660 =input/ >> ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo mouse.* 0:0 0660 =input/ >> ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo event.* 0:0 0660 =input/ >> ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo pcm.* 0:0 0660 =snd/ >> ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo control.* 0:0 0660 =snd/ >> ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo timer 0:0 0660 =snd/ >> ${OUTPUTDIR}/rootfs/etc/mdev.conf
	echo '$$DEVNAME=bus/usb/([0-9]+)/([0-9]+) 0:0 0660 =bus/usb/%1/%2' >> ${OUTPUTDIR}/rootfs/etc/mdev.conf

	echo export PATH=\$$PATH:/config >> ${OUTPUTDIR}/rootfs/etc/profile
	echo export TERMINFO=/config/terminfo >> ${OUTPUTDIR}/rootfs/etc/profile
	sed -i '/^mount.*/d' $(OUTPUTDIR)/rootfs/etc/profile
	echo mkdir -p /dev/pts >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS
	echo export TZ=`cat /etc/TZ` >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS
	echo mount -t sysfs none /sys >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo mount -t tmpfs mdev /dev >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo mount -t debugfs none /sys/kernel/debug/ >>  $(OUTPUTDIR)/rootfs/etc/init.d/rcS

	cp -rvf $(PROJ_ROOT)/tools/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/fw_printenv/* $(OUTPUTDIR)/rootfs/etc/
	echo "$(ENV_CFG)" > $(OUTPUTDIR)/rootfs/etc/fw_env.config
	cd $(OUTPUTDIR)/rootfs/etc/;ln -sf fw_printenv fw_setenv
	echo mkdir -p /var/lock >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS

	if [ "$(FLASH_TYPE)"x = "nor"x  ]; then \
		echo mdev -s >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS ;\
	fi;
	echo -e $(foreach block, $(USR_MOUNT_BLOCKS), "mount -t $($(block)$(FSTYPE)) $($(block)$(MOUNTPT)) $($(block)$(MOUNTTG))\n") >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS

	-chmod 755 $(LIB_DIR_PATH)/bin/debug/*
	cp -rf $(LIB_DIR_PATH)/bin/debug/* $(OUTPUTDIR)/rootfs/customer/
	#add:  remove sshd in nor flash default
	if [[ "$(FLASH_TYPE)"x = "nor"x ]] && [[ -d "$(OUTPUTDIR)/rootfs/customer/ssh" ]]; then \
		rm -rf $(OUTPUTDIR)/rootfs/customer/ssh; \
	fi;
	#end add

	if [ -f "$(OUTPUTDIR)/rootfs/customer/demo.sh" ]; then \
		rm  $(OUTPUTDIR)/rootfs/customer/demo.sh; \
	fi;
	touch $(OUTPUTDIR)/rootfs/customer/demo.sh
	chmod 755 $(OUTPUTDIR)/rootfs/customer/demo.sh

	# change linux printk level to 4 
	echo 'echo 4 > /proc/sys/kernel/printk' >> $(OUTPUTDIR)/rootfs/customer/demo.sh
	echo >> $(OUTPUTDIR)/rootfs/customer/demo.sh

	# creat insmod ko scrpit
	if [ -f "$(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/kernel_mod_list" ]; then \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/kernel_mod_list | sed 's#\(.*\).ko#insmod /config/modules/$(KERNEL_VERSION)/\1.ko#' > $(OUTPUTDIR)/rootfs/customer/demo.sh; \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/kernel_mod_list | sed 's#\(.*\).ko\(.*\)#$(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/\1.ko#' | xargs -i cp -rvf {} $(miservice$(RESOUCE))/modules/$(KERNEL_VERSION); \
		echo "#kernel_mod_list" >> $(OUTPUTDIR)/rootfs/customer/demo.sh; \
	fi;

	if [ "$(DUAL_OS)" != "on" ]; then \
		if [ -f "$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/misc_mod_list" ]; then \
			cat $(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/misc_mod_list | sed 's#\(.*\).ko#insmod /config/modules/$(KERNEL_VERSION)/\1.ko#' >> $(OUTPUTDIR)/rootfs/customer/demo.sh; \
			cat $(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/misc_mod_list | sed 's#\(.*\).ko\(.*\)#$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/\1.ko#' | xargs -i cp -rvf {} $(miservice$(RESOUCE))/modules/$(KERNEL_VERSION); \
			echo "#misc_mod_list" >> $(OUTPUTDIR)/rootfs/customer/demo.sh; \
		fi; \
		if [ -f "$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/.mods_depend" ]; then \
			cat $(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/.mods_depend | sed '2,20s#\(.*\)#insmod /config/modules/$(KERNEL_VERSION)/\1.ko\nif [ $$? -eq 0 ]; then\n	busybox mknod /dev/\1 c $$major $$minor\n	let minor++\nfi\n\n#' >> $(OUTPUTDIR)/rootfs/customer/demo.sh; \
			cat $(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/.mods_depend | sed 's#\(.*\)#$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/\1.ko#' | xargs -i cp -rvf {} $(miservice$(RESOUCE))/modules/$(KERNEL_VERSION); \
			echo "#mi module" >> $(OUTPUTDIR)/rootfs/customer/demo.sh; \
		fi; \
		if [ -f "$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/misc_mod_list_late" ]; then \
			cat $(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/misc_mod_list_late | sed 's#\(.*\).ko#insmod /config/modules/$(KERNEL_VERSION)/\1.ko#' >> $(OUTPUTDIR)/rootfs/customer/demo.sh; \
			cat $(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/misc_mod_list_late | sed 's#\(.*\).ko\(.*\)#$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/\1.ko#' | xargs -i cp -rvf {} $(miservice$(RESOUCE))/modules/$(KERNEL_VERSION); \
			echo "#misc_mod_list_late" >> $(OUTPUTDIR)/rootfs/customer/demo.sh; \
		fi; \
	fi;

	if [ -f "$(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/kernel_mod_list_late" ]; then \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/kernel_mod_list_late | sed 's#\(.*\).ko#insmod /config/modules/$(KERNEL_VERSION)/\1.ko#' >> $(OUTPUTDIR)/rootfs/customer/demo.sh; \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/kernel_mod_list_late | sed 's#\(.*\).ko\(.*\)#$(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/\1.ko#' | xargs -i cp -rvf {} $(miservice$(RESOUCE))/modules/$(KERNEL_VERSION); \
		echo "#kernel_mod_list_late" >> $(OUTPUTDIR)/rootfs/customer/demo.sh; \
	fi;

	if [ "$(DUAL_OS)" != "on" ]; then \
		if [ "$(SENSOR_LIST)" != "" ]; then \
			cp -rvf $(foreach n,$(SENSOR_LIST),$(LIB_DIR_PATH)/modules/$(KERNEL_VERSION)/$(n)) $(miservice$(RESOUCE))/modules/$(KERNEL_VERSION); \
		fi; \
		if [ "$(SENSOR0)" != "" ]; then \
			echo insmod /config/modules/$(KERNEL_VERSION)/$(SENSOR0) $(SENSOR0_OPT) >> $(OUTPUTDIR)/rootfs/customer/demo.sh; \
		fi; \
		if [ "$(SENSOR1)" != "" ]; then \
			echo insmod /config/modules/$(KERNEL_VERSION)/$(SENSOR1) $(SENSOR1_OPT) >> $(OUTPUTDIR)/rootfs/customer/demo.sh; \
		fi; \
		if [ "$(SENSOR2)" != "" ]; then \
			echo insmod /config/modules/$(KERNEL_VERSION)/$(SENSOR2) $(SENSOR2_OPT) >> $(OUTPUTDIR)/rootfs/customer/demo.sh; \
		fi;	\
		sed -i 's/mi_sys.ko/mi_sys.ko cmdQBufSize=768 logBufSize=256/g' $(OUTPUTDIR)/rootfs/customer/demo.sh; \
		sed -i '/mi_iqserver.ko/,+4d' $(OUTPUTDIR)/rootfs/customer/demo.sh;\
		sed -i '/mi_isp.ko/,+4d' $(OUTPUTDIR)/rootfs/customer/demo.sh;\
		sed -i 's/mi_common/insmod \/config\/modules\/$(KERNEL_VERSION)\/mi_common.ko\nmajor=\`cat \/proc\/devices \| busybox awk "\\\\$$2==\\""mi"\\" {print \\\\$$1}"\\n`\nminor=0/g' $(OUTPUTDIR)/rootfs/customer/demo.sh; \
		sed -i '/#mi module/a	major=`cat /proc/devices | busybox awk "\\\\$$2==\\""mi_poll"\\" {print \\\\$$1}"`\nbusybox mknod \/dev\/mi_poll c $$major 0' $(OUTPUTDIR)/rootfs/customer/demo.sh; \
	fi;

	if [ $(PHY_TEST) = "yes" ]; then \
		echo -e "\033[41;33;5m !!! Replace "mdrv-sata-host.ko" with "sata_bench_test.ko" !!!\033[0m" ; \
		sed '/mdrv-sata-host/d' $(OUTPUTDIR)/rootfs/customer/demo.sh > $(OUTPUTDIR)/rootfs/customer/demotemp.sh ; \
		echo insmod /config/sata_phy/sata_bench_test.ko >> $(OUTPUTDIR)/rootfs/customer/demotemp.sh ; \
		cp $(OUTPUTDIR)/rootfs/customer/demotemp.sh $(OUTPUTDIR)/rootfs/customer/demo.sh ; \
		rm $(OUTPUTDIR)/rootfs/customer/demotemp.sh ; \
	fi;

	# Enable MIU protect on CMDQ buffer as default (While List: CPU)
	# [I5] The 1st 1MB of MIU is not for CMDQ buffer
#	echo 'echo set_miu_block3_status 0 0x70 0 0x100000 1 > /proc/mi_modules/mi_sys_mma/miu_protect' >> $(OUTPUTDIR)/rootfs/customer/demo.sh

#	echo mount -t jffs2 /dev/mtdblock3 /config >> $(OUTPUTDIR)/rootfs/etc/profile
	rm -rf $(OUTPUTDIR)/rootfs/lib/modules/*
	ln -fs /config/modules/$(KERNEL_VERSION) $(OUTPUTDIR)/rootfs/lib/modules/
	find $(miservice$(RESOUCE))/modules/$(KERNEL_VERSION) -name "*.ko" | xargs $(TOOLCHAIN_REL)strip  --strip-unneeded;
	find $(OUTPUTDIR)/rootfs/lib/ -name "*.so" | xargs $(TOOLCHAIN_REL)strip  --strip-unneeded;
	echo mkdir -p /dev/pts >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo mount -t devpts devpts /dev/pts >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "busybox telnetd&" >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
        
	echo "#Camera PowerUp sequence " >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "echo  66 > /sys/class/gpio/export " >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "echo  out > /sys/class/gpio/gpio66/direction " >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "echo  1 > /sys/class/gpio/gpio66/value " >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "sleep 0.01 " >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "echo  64 > /sys/class/gpio/export " >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "echo  out > /sys/class/gpio/gpio64/direction " >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "echo  1 > /sys/class/gpio/gpio64/value " >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "sleep 0.01 " >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "echo  65 > /sys/class/gpio/export " >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "echo  out > /sys/class/gpio/gpio65/direction " >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "echo  1 > /sys/class/gpio/gpio65/value " >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS


	echo "if [ -e /customer/demo.sh ]; then" >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "    /customer/demo.sh" >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo "fi;" >> $(OUTPUTDIR)/rootfs/etc/init.d/rcS
	echo mdev -s >> $(OUTPUTDIR)/rootfs/customer/demo.sh
	if [ $(BENCH) = "yes" ]; then \
		echo ./config/bench.sh >> $(OUTPUTDIR)/rootfs/customer/demo.sh ; \
	fi;
	#add sshd, default password 1234
	if [[ "$(FLASH_TYPE)"x = "spinand"x ]]; then \
		if [[ $(TOOLCHAIN_VERSION) = "9.1.0" ]] || [[ $(TOOLCHAIN_VERSION) = "8.2.1" ]]; then \
			echo "root:5fXKAeftHX95A:0:0:Linux User,,,:/home/root:/bin/sh" > $(OUTPUTDIR)/rootfs/etc/passwd; \
			echo "sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin" >> $(OUTPUTDIR)/rootfs/etc/passwd; \
			echo "www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin" >> $(OUTPUTDIR)/rootfs/etc/passwd; \
			echo "export LD_LIBRARY_PATH=\$$LD_LIBRARY_PATH:/customer/ssh/lib" >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS; \
			echo "mkdir /var/empty" >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS; \
			echo "/customer/ssh/sbin/sshd -f /customer/ssh/etc/sshd_config" >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS; \
			echo "export LD_LIBRARY_PATH=\$$LD_LIBRARY_PATH:/customer/ssh/lib" >> ${OUTPUTDIR}/rootfs/etc/profile; \
		        echo "mkdir /var/run" >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS; \
			echo ". /etc/rc.local" >> ${OUTPUTDIR}/rootfs/etc/init.d/rcS; \
		fi; \
	fi;
	#end add

	mkdir -p $(OUTPUTDIR)/vendor
	mkdir -p $(OUTPUTDIR)/rootfs/customer
	mkdir -p $(OUTPUTDIR)/rootfs/vendor
	mkdir -p $(OUTPUTDIR)/rootfs/customer
	mkdir -p $(OUTPUTDIR)/bootconfig
	mkdir -p $(OUTPUTDIR)/rootfs/bootconfig
	if [ $(ramdisk$(RESOUCE)) != "" ]; then \
		rm -rf $(RAMDISK_DIR); \
		mkdir -p $(RAMDISK_DIR); \
		cd $(RAMDISK_DIR); cpio -i -F $(RAMDISK_IMG); \
		cp -R $(OUTPUTDIR)/tmprd/linuxrc $(OUTPUTDIR)/tmprd/init; \
		rm -rf $(OUTPUTDIR)/tmprd/usr/lib/modules/*; \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/ramdisk_rc1 | sed 's#\(.*\).ko\(.*\)#$(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/\1.ko#' | xargs -i cp -rvf {} $(OUTPUTDIR)/tmprd/usr/lib/modules; \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/ramdisk_rc2 | sed 's#\(.*\).ko\(.*\)#$(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/\1.ko#' | xargs -i cp -rvf {} $(OUTPUTDIR)/tmprd/usr/lib/modules; \
		$(PREFIX)strip --strip-unneeded $(OUTPUTDIR)/tmprd/usr/lib/modules/*; \
		cd $(RAMDISK_DIR); find | cpio -o -H newc -O $(RAMDISK_IMG); \
		echo "#!/bin/sh" > $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "echo Console done" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mount -a" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		cat $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION)/$(CHIP)/configs/$(PRODUCT)/$(BOARD)/$(TOOLCHAIN)/$(TOOLCHAIN_VERSION)/$(FLASH_TYPE)/modules/ramdisk_rc2 | sed 's#\(.*\).ko#insmod /usr/lib/modules/\1.ko#' >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "ubiattach /dev/ubi_ctrl -m $(UBI_AT_MTD)" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mount -t ubifs ubi0:rootfs /usr" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mount -t ubifs ubi0:miservice /config" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mount -t ubifs ubi0:customer /customer" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mkdir -p /dev/shm" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mkdir -p /dev/pts" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mount -t devpts for_telnetd /dev/pts" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "/usr/usr/sbin/telnetd -l /usr/bin/ash" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "mount --bind /usr/bin/sh /bin/sh" >> $(OUTPUTDIR)/tmprd/etc/init.d/rcS; \
		echo "#!/bin/sh" > $(OUTPUTDIR)/tmprd/etc/profile; \
		echo "export PATH=/usr/bin:/usr/sbin:/bin:/sbin:/usr/usr/bin:/usr/usr/sbin" >> $(OUTPUTDIR)/tmprd/etc/profile; \
		echo "/usr/bin/busybox sh /customer/demo.sh" >> $(OUTPUTDIR)/tmprd/etc/profile; \
		echo export TERMINFO=/config/terminfo >> ${OUTPUTDIR}/tmprd/etc/profile; \
		cd $(PROJ_ROOT)/kbuild/$(KERNEL_VERSION); \
		./scripts/gen_initramfs_list.sh -o $(OUTPUTDIR)/initramfs.gz -u 0 -g 0 $(OUTPUTDIR)/tmprd/; \
	fi;
