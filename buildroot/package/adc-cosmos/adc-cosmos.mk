ADC_COSMOS_VERSION = develop
ADC_COSMOS_SITE = https://buildroot:buildroot@bitbucket.org/PixcellenceTech/cosmos.git
ADC_COSMOS_SITE_METHOD = git
ADC_COSMOS_INSTALL_STAGING = yes
ADC_COSMOS_INSTALL_TARGET = YES
ADC_COSMOS_DEPENDENCIES = util-linux libfcgi wpa_supplicant iperf  iperf3 libcurl openssl cjson czmq  sqlite dl  
ADC_COSMOS_PLATFORM := $(call qstrip,$(BR2_TARGET_COSMOS_PLATFORM))
ADC_COSMOS_SOCNAME := $(call qstrip,$(BR2_TARGET_COSMOS_SOCNAME))


define ADC_COSMOS_CONFIGURE_CMDS
	(cd $(@D); sed  -i  's+TESTS=yes+TESTS=no+g' build_project.sh)
	(cd $(@D); sed  -i  's+set(TOOLCHAIN_BINDIR.*+\
	set(TOOLCHAIN_BINDIR $(HOST_DIR)/opt/ext-toolchain/bin/)\n \
	set(CMAKE_SYSROOT $(HOST_DIR)/arm-buildroot-linux-gnueabihf/sysroot/)+g'  cmake/toolchain-sav530.cmake	 )  
	(cd $(@D); find . -type f -name "*.txt" -exec sed  -i  's+($${CMAKE_SOURCE_DIR}/libs/$${PLATFORM_NAME}/lib)+\
	($(HOST_DIR)/arm-buildroot-linux-gnueabihf/sysroot/usr/lib $(HOST_DIR)/arm-buildroot-linux-gnueabihf/sysroot/lib \
	$(HOST_DIR)/../target/usr/lib)+g' {} + )
        (cd $(@D); cp -a libs/sav530/lib/libg3logger.so* $(HOST_DIR)/arm-buildroot-linux-gnueabihf/sysroot/lib/)
        (cd $(@D); sed -i  's+wpa_cli+wpa_client+g' apps/network/CMakeLists.txt)	
        (cd $(@D); sed -i  's+set(LINK_LIBRARIES ${LINK_LIBRARIES} busybox)+d+g' apps/cgiserver/CMakeLists.txt)	
endef

define ADC_COSMOS_BUILD_CMDS
	(cd $(@D); $(@D)/build_project.sh  $(ADC_COSMOS_PLATFORM) $(ADC_COSMOS_SOCNAME))
endef

define ADC_COSMOS_INSTALL_TARGET_CMDS
	cp $(@D)/target/bin/*  $(TARGET_DIR)/usr/bin/.
	cp -a $(@D)/target/lib/*  $(TARGET_DIR)/usr/lib/. 	
endef


$(eval $(generic-package))
