*************************************************
Mender integration on sigmastar SAV530 EVK board
*************************************************


Build steps for sav530 board
=====

1. checkout the latest code
   git clone https://<user-id>@bitbucket.org/PixcellenceTech/adc-buildroot.git
   cd /path-to/adc-buildroot
   git remote set-head  origin develop
   git pull origin develop
   git checkout develop
   

2. generate the configuration file,
   cd /path-to/adc-buildroot/
  ./buildroot-external-mender/board/sigmastar/sav530/gen-defconfig.sh

3 Configure Buildroot for the SAV530 EVK board:
   make mender_sigmastar_sav530_defconfig

4, Build all images
   make 
   Note: buildroot ask passwd for linux build, press enter to continue
   
   Once the build is successfull,o/p images is placed in 
   /path-to/bsp-sav530/buildroot-mender/buildroot/output/images
  
       - contents of images folder
   
    	.
	├── auto_update.txt
	├── boot
	│   ├── GCIS.bin
	│   ├── IPL.bin
	│   ├── IPL_CUST.bin
	│   ├── PARTINFO.pni
	│   ├── SPINANDINFO.sni	
	│   └── u-boot_spinand.xz.img.bin
	├── ipl_cust_s.bin
	├── ipl_s.bin
	├── kernel
	├── release-1.mender
	├── rootfs.squashfs
	├── scripts
	│   ├── [[ipl_cust.es
	│   ├── [[ipl.es
	│   ├── [[kernel.es
	│   ├── [[rootfs.es
	│   ├── set_config
	│   ├── [[set_partition.es
	│   └── [[uboot.es
    	└── uboot_s.bin


    - Build  mender components:

      make mender-rebuild

    - Build sigmastar kernel and modules

      make linux-rebuild

5. Flashing images.
   - copy images folder to windows host.
   - run tftserver on host,click its Browse button and navigate to images folder.
   - boot sav530 till uboot promt.
   - type 'estar', enter to flash images. 
   - Once the images are sucessfully flashed, the system reboot and stop at linux bash shell promt.

6.  running the app on sav530.
   - create a persistent storage on sav530
      . /etc/setup.sh (setup script)
   - copy app to persistent storage from tftpserver
      ensure tftpserver in running and folder has the mages to be flashed)
     /alarm> tftp -g -r <filename> IP-TFTPSERVER-HOST 
           e,g tftp -g -r filename.txt 192.168.7.144
        

